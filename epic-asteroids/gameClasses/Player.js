var Player = IgeEntityBox2d.extend({
	classId: 'Player',

	init: function () {
		IgeEntityBox2d.prototype.init.call(this);

		var self = this;
		self.fireDelay = 500; // ms
		self.lastFireTime = 0;
		
		
		this.drawBounds(true);

		// Rotate to point upwards
		this.controls = {
			left: false,
			right: false,
			thrust: false,
			fire: false
		};
		
		self.width(20);
		self.height(20);
		
		if (ige.isServer) {
			this.addComponent(IgeVelocityComponent);
			self.box2dBody({
					type: 'dynamic',
					linearDamping: 0.0,
					angularDamping: 0.1,
					allowSleep: true,
					bullet: false,
					gravitic: false,
					fixedRotation: false,
					fixtures: [{
						density: 1.0,
						friction: 0.1,
						restitution: 0.2,
						shape: {
							type: 'rectangle'
						}
					}]
				});
		}
		
		if (!ige.isServer) {
			self.texture(ige.client.textures.ship)
			.width(self.width())
			.height(self.height());
		}
		
		

		// Define the data sections that will be included in the stream
		this.streamSections(['transform', 'score']);
	},

	/**
	 * Override the default IgeEntity class streamSectionData() method
	 * so that we can check for the custom1 section and handle how we deal
	 * with it.
	 * @param {String} sectionId A string identifying the section to
	 * handle data get / set for.
	 * @param {*=} data If present, this is the data that has been sent
	 * from the server to the client for this entity.
	 * @return {*}
	 */
	streamSectionData: function (sectionId, data) {
		// Check if the section is one that we are handling
		if (sectionId === 'score') {
			// Check if the server sent us data, if not we are supposed
			// to return the data instead of set it
			if (data) {
				// We have been given new data!
				this._score = data;
			} else {
				// Return current data
				return this._score;
			}
		} else {
			// The section was not one that we handle here, so pass this
			// to the super-class streamSectionData() method - it handles
			// the "transform" section by itself
			return IgeEntity.prototype.streamSectionData.call(this, sectionId, data);
		}
	},

	/**
	 * Called every frame by the engine when this entity is mounted to the
	 * scenegraph.
	 * @param ctx The canvas context to render to.
	 */
	tick: function (ctx) {
		/* CEXCLUDE */
		if (ige.isServer) {
			if (this.controls.left) {
				this.rotateBy(0, 0, Math.radians(-0.2 * ige._tickDelta));
			}

			if (this.controls.right) {
				this.rotateBy(0, 0, Math.radians(0.2 * ige._tickDelta));
			}

			if (this.controls.thrust) {
				this.velocity.byAngleAndPower(this._rotate.z + Math.radians(-90), 0.1);
			} else {
				this.velocity.x(0);
				this.velocity.y(0);
			}
			
			if (this.controls.fire) {
				var sinceLastFire = new Date().getTime() - this.lastFireTime;
				
				if (sinceLastFire >= this.fireDelay) {
					var pos = this.worldPosition();
					console.log(new Date().getTime() + ' - FIRE ' + pos.x + ', ' + pos.y);
					var firePos = pos.clone();
					firePos.y -= (this.height() / 2) + 3;
					var x = firePos.x;
					var y = firePos.y;
					firePos.x = Math.cos(this._rotate.z) * (x - pos.x)
						- Math.sin(this._rotate.z) * (y - pos.y) + pos.x;
					firePos.y = Math.sin(this._rotate.z) * (x - pos.x)
						+ Math.cos(this._rotate.z) * (y - pos.y) + pos.y;
					
					console.log('rotate: ' + JSON.stringify(this._rotate));
					
					var bullet = new Circle()
						.translateTo(firePos.x, firePos.y, 0)
						.drawBounds(true)
						.streamMode(1)
						.lifeSpan(10000)
						.mount(ige.server.scene1);
					bullet.velocity.byAngleAndPower(this._rotate.z + Math.radians(-90), 0.3);
					
					this.lastFireTime = new Date().getTime();
				}
				
			}
		}
		/* CEXCLUDE */

		if (!ige.isServer) {
			if (ige.input.actionState('left')) {
				if (!this.controls.left) {
					// Record the new state
					this.controls.left = true;

					// Tell the server about our control change
					ige.network.send('playerControlLeftDown');
				}
			} else {
				if (this.controls.left) {
					// Record the new state
					this.controls.left = false;

					// Tell the server about our control change
					ige.network.send('playerControlLeftUp');
				}
			}

			if (ige.input.actionState('right')) {
				if (!this.controls.right) {
					// Record the new state
					this.controls.right = true;

					// Tell the server about our control change
					ige.network.send('playerControlRightDown');
				}
			} else {
				if (this.controls.right) {
					// Record the new state
					this.controls.right = false;

					// Tell the server about our control change
					ige.network.send('playerControlRightUp');
				}
			}

			if (ige.input.actionState('thrust')) {
				if (!this.controls.thrust) {
					// Record the new state
					this.controls.thrust = true;

					// Tell the server about our control change
					ige.network.send('playerControlThrustDown');
				}
			} else {
				if (this.controls.thrust) {
					// Record the new state
					this.controls.thrust = false;

					// Tell the server about our control change
					ige.network.send('playerControlThrustUp');
				}
			}
			
			if (ige.input.actionState('fire')) {
				if (!this.controls.fire) {
					// Record the new state
					this.controls.fire = true;

					// Tell the server about our control change
					ige.network.send('playerControlFireDown');
				}
			} else {
				if (this.controls.fire) {
					// Record the new state
					this.controls.fire = false;

					// Tell the server about our control change
					ige.network.send('playerControlFireUp');
				}
			}
		}

		// Call the IgeEntity (super-class) tick() method
		IgeEntityBox2d.prototype.tick.call(this, ctx);
	}
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = Player; }