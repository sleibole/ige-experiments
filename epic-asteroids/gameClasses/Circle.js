var Circle = IgeEntityBox2d.extend({
	classId: 'Circle',

	init: function () {
		IgeEntityBox2d.prototype.init.call(this);

		var self = this;
		
		
		self.width(5);
		self.height(5);

		if (ige.isServer) {
			self.addComponent(IgeVelocityComponent);
			self.box2dBody({
				type: 'dynamic',
				linearDamping: 0.0,
				angularDamping: 0.1,
				allowSleep: true,
				bullet: true,
				gravitic: false,
				fixedRotation: false,
				fixtures: [{
					density: 1.0,
					friction: 0.5,
					restitution: 0.2,
					shape: {
						type: 'circle'
					}
				}]
			});
		}

		if (!ige.isServer) {
			// Define the texture this entity will use
			self.texture(ige.client.textures.circle)
				.width(self.width())
				.height(self.height());
		}
		
		this.streamSections(['transform', 'geometry']);
	},

	tick: function (ctx) {
		IgeEntityBox2d.prototype.tick.call(this, ctx);
	}
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = Circle; }